using System.Collections.Generic;
using System.Linq;
using DetentionCentre.Models;
using DetentionCentre.Repositories;
using DetentionCentre.Services;
using Moq;
using NUnit.Framework;

namespace DetentionCentre.UnitTests
{
    public class DetentionManagementServiceTests
    {
        private Mock<IDetaineeRepository> _detaineeRepositoryMock;
        private Mock<IArrestRepository> _arrestRepositoryMock;
        private Mock<IOfficersRepository> _officersRepositoryMock;

        private IEnumerable<Officer> allOfficers = new List<Officer>
        {
            new Officer
            {
                OfficerId = 0,
                FirstName = "First1",
                LastName = "Last1",
                MiddleName = "M1",
                Title = "Title1"
            },
            new Officer
            {
                OfficerId = 1,
                FirstName = "First2",
                LastName = "Last2",
                MiddleName = "M2",
                Title = "Title2"
            },
        };

        [SetUp]
        public void Setup()
        {
            _detaineeRepositoryMock = new Mock<IDetaineeRepository>();

            _arrestRepositoryMock = new Mock<IArrestRepository>();

            _officersRepositoryMock = new Mock<IOfficersRepository>();
            _officersRepositoryMock.Setup(s => s.GetOfficers()).Returns(allOfficers);
        }

        [Test]
        public void TestGetAvailableOfficersWithExclude()
        {
            var detentionManagementService = new DetentionManagementService(
                _detaineeRepositoryMock.Object,
                _arrestRepositoryMock.Object,
                _officersRepositoryMock.Object);

            List<Officer> inputOfficers = new List<Officer>
            {
                new Officer
                {
                    OfficerId = 0,
                    FirstName = "First1",
                    LastName = "Last1",
                    MiddleName = "M1",
                    Title = "Title1"
                },
            };

            var result = detentionManagementService.GetAvailableOfficers(inputOfficers);

            Assert.NotNull(result);
            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("First2", result.First().FirstName);
        }

        [Test]
        public void TestGetAvailableOfficersAll()
        {
            var detentionManagementService = new DetentionManagementService(
                _detaineeRepositoryMock.Object,
                _arrestRepositoryMock.Object, _officersRepositoryMock.Object);

            var result = detentionManagementService.GetAvailableOfficers();

            Assert.NotNull(result);
            Assert.AreEqual(2, result.Count());
        }
    }
}