﻿using DetentionCentre.Extensions;
using DetentionCentre.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DetentionCentre.UnitTests
{
    public class CollectionsExtentionTests
    {
        [Test]
        public void TestIntersectNonEmptyWithBothEmpty()
        {
            var firstCollection = new List<Detainee>();
            var secondCollection = new List<Detainee>();

            var result = firstCollection.IntersectNonEmpty(secondCollection);
            var expectedResult = secondCollection;

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void TestIntersectNonEmptyWithFirstEmpty()
        {
            var firstCollection = new List<Detainee>();
            var secondCollection = new List<Detainee>{
                new Detainee
                {
                    DetaineeId = 1,
                    LastName = "Kalinivski",
                    FirstName = "Konstantin",
                    MiddleName = "Semionovich",
                    BirthDate = new DateTime(1838, 02, 02),
                    MaritalStatus = "single",
                    HomeAdress = "Vilna",
                    PhoneNumber = null,
                    Photo = null,
                    WorkPlace = "Nasha Niva",
                    Arrests = null,
                    Additional = "revolutionary democrat"
                } };

            var result = firstCollection.IntersectNonEmpty(secondCollection);
            var expectedResult = secondCollection;

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void TestIntersectNonEmptyWithSecondEmpty()
        {
            var firstCollection = new List<Detainee>
            {
                new Detainee
                {
                    DetaineeId = 2,
                    LastName = "Lutsevich",
                    FirstName = "Ivan",
                    MiddleName = "Daminikavich",
                    BirthDate = new DateTime(1882, 06, 28),
                    MaritalStatus = "single",
                    HomeAdress = "Vilna",
                    PhoneNumber = null,
                    Photo = null,
                    WorkPlace = "Nasha Niva",
                    Arrests = null,
                    Additional = "poet and writer"
                }
            };

            var secondCollection = new List<Detainee>();

            var result = firstCollection.IntersectNonEmpty(secondCollection);
            var expectedResult = firstCollection;

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void TestIntersectNonEmptyWithBothFilled()
        {
            var kalinouski = new Detainee
            {
                DetaineeId = 1,
                LastName = "Kalinivski",
                FirstName = "Konstantin",
                MiddleName = "Semionovich",
                BirthDate = new DateTime(1838, 02, 02),
                MaritalStatus = "single",
                HomeAdress = "Vilna",
                PhoneNumber = null,
                Photo = null,
                WorkPlace = "Nasha Niva",
                Arrests = null,
                Additional = "revolutionary democrat"
            };

            var firstCollection = new List<Detainee>
            {
                kalinouski
            };

            var secondCollection = new List<Detainee>
            {
               kalinouski
            };

            var result = firstCollection.IntersectNonEmpty(secondCollection).ToList();
            var expectedResult = new List<Detainee> { kalinouski };

            for (int i = 0; i < expectedResult.Count(); i++)
            {
                Assert.AreEqual(expectedResult[i], result[i]);
            }

        }
    }
}
