﻿using DetentionCentre.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DetentionCentre.ViewModels
{
    public class SearchViewModel
    {
        [MaxLength(50)]
        public string FirstName { get; set; }

        [MaxLength(50)]
        public string LastName { get; set; }

        [MaxLength(50)]
        public string MiddleName { get; set; }

        [MaxLength(150)]
        public string HomeAdress { get; set; }

        public DateTime? DetentionDate { get; set; }

        public IEnumerable<Detainee> DetaineesList { get; set; }
    }
}
