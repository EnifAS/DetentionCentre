﻿using Microsoft.AspNetCore.Http;

namespace DetentionCentre.ViewModels
{
    public class FileUploadViewModel
    {
        public int DetaineeId { get; set; }

        public IFormFile UploadedFile { get; set; }
    }
}
