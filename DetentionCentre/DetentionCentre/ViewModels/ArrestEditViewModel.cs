﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DetentionCentre.Models;

namespace DetentionCentre.ViewModels
{
    public class ArrestEditViewModel
    {
        public int? ArrestId { get; set; }

        [Required(ErrorMessage = "Indicate detention date")]
        public DateTime? DetentionDate { get; set; }

        [Required(ErrorMessage = "Indicate delivery date")]
        public DateTime? DeliveryDate { get; set; }

        public DateTime? ReleaseDate { get; set; }

        [Required(ErrorMessage = "Enter detention place")]
        [MaxLength(150)]
        public string DetentionPlace { get; set; }

        [RegularExpression("^[0-9]+(\\.[0-9]{1,2})?$", ErrorMessage = "Allowed only decimals with double precision separated by dot.")]
        public string EstimatedAmount { get; set; }

        [RegularExpression("^[0-9]+(\\.[0-9]{1,2})?$", ErrorMessage = "Allowed only decimals with double precision separated by dot.")]
        public string PaidAmount { get; set; }

        public int DetaineeId { get; set; }

        public IList<Officer> Officers { get; set; }

        public IEnumerable<Officer> AvailableOfficers { get; set; }

        public int? SelectedOfficerId { get; set; }
    }
}
