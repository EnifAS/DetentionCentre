﻿using DetentionCentre.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DetentionCentre.ViewModels
{
    public class OfficersViewModel
    {
        public int? OfficerId { get; set; }

        [Required(ErrorMessage = "Enter last name")]
        [MaxLength(50)]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Enter first name")]
        [MaxLength(50)]
        public string FirstName { get; set; }

        [MaxLength(50)]
        public string MiddleName { get; set; }

        [Required(ErrorMessage = "Enter title")]
        [MaxLength(50)]
        public string Title { get; set; }

        public IEnumerable<Officer> AllOfficers { get; set; }
    }
}
