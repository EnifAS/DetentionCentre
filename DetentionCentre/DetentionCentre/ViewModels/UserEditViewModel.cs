﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DetentionCentre.Models;

namespace DetentionCentre.ViewModels
{
    public class UserEditViewModel
    {
        public int? Id { get; set; }

        [MaxLength(50)]
        public string Login { get; set; }

        [MaxLength(100)]
        public string Password { get; set; }

        [MaxLength(100)]
        public string Email { get; set; }

        public int? RoleId { get; set; }

        public IEnumerable<Role> AvailableRoles { get; set; }
    }
}
