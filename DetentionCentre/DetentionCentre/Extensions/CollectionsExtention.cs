﻿using System.Collections.Generic;
using System.Linq;

namespace DetentionCentre.Extensions
{
    public static class CollectionsExtention
    {
        public static IEnumerable<T> IntersectNonEmpty<T>(this IEnumerable<T> firstList, IEnumerable<T> secondList)
        {
            if (!firstList.Any())
            {
                return secondList.ToList();
            }
            else if (!secondList.Any())
            {
                return firstList.ToList();
            }

            return firstList.Intersect(secondList);
        }
    }
}
