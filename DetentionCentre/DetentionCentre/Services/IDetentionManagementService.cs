﻿using DetentionCentre.Models;
using DetentionCentre.ViewModels;
using System.Collections.Generic;

namespace DetentionCentre.Services
{
    public interface IDetentionManagementService
    {
        IEnumerable<Detainee> GetDetainees();

        Detainee GetDetaineeById(int detaineeId);

        void DeleteDetaineeById(int detaineeId, string pathToPhotos);

        int CreateDetainee(Detainee detainee);

        void DeleteArrestByArrestId(int arrestId);

        void UpdatePhoto(int detaineeId, string pathToPhoto);

        void UpdateDetaineeInformation(Detainee detainee);

        int InsertArrest(Arrest arrest);

        void UpdateArrest(Arrest arrest);

        Arrest GetArrestByArrestID(int arrestId);

        void InsertOfficerForArrest(Officer officer, int arrestId);

        public IEnumerable<int> GetLinks(int arrestId);

        public void AddLinks(int arrestId, IEnumerable<Officer> officers);

        public void DeleteLinks(int arrestId, IEnumerable<Officer> officers);

        public IEnumerable<Officer> GetOfficers();

        public void CreateOfficer(Officer officer);

        List<Detainee> SearchDetainees(SearchViewModel model);

        IEnumerable<Officer> GetAvailableOfficers(IEnumerable<Officer> arrestOfficers = null);

        Officer GetOfficerById(int officerId);

        void DeleteOfficer(int officerId);

        void UpdateOfficer(Officer officer);
    }
}
