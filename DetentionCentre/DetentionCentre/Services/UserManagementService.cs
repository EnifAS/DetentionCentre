﻿using DetentionCentre.Models;
using DetentionCentre.Repositories;
using DetentionCentre.ViewModels;
using System.Collections.Generic;

namespace DetentionCentre.Services
{
    public class UserManagementService : IUserManagementService
    {
        private readonly IUserRepository _userRepository;

        public UserManagementService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public int CreateUser(RegisterModel registerModel)
        {
            return _userRepository.CreateUser(registerModel);
        }

        public User GetUser(string login, string password)
        {
            return _userRepository.GetUser(login, password);
        }

        public User GetUser(int id)
        {
            return _userRepository.GetUser(id);
        }

        public bool IsUserExist(string login, string email)
        {
            return _userRepository.IsUserExist(login, email);
        }

        public IEnumerable<Role> GetRoles()
        {
            return _userRepository.GetRoles();
        }

        public IEnumerable<User> GetUsers()
        {
            return _userRepository.GetUsers();
        }

        public void SaveChanges(User user)
        {
            _userRepository.SaveChanges(user);
        }

        public void Delete(int userId)
        {
            _userRepository.Delete(userId);
        }

        public void AddUser(UserEditViewModel model)
        {
            _userRepository.AddUser(model);
        }
    }
}
