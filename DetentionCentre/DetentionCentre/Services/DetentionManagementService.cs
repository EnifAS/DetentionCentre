﻿using DetentionCentre.Models;
using DetentionCentre.Repositories;
using DetentionCentre.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace DetentionCentre.Services
{
    public class DetentionManagementService : IDetentionManagementService
    {
        private readonly IDetaineeRepository _detaineeRepository;
        private readonly IArrestRepository _arrestRepository;
        private readonly IOfficersRepository _officersRepository;

        public DetentionManagementService(IDetaineeRepository detaineeRepository, IArrestRepository arrestRepository, IOfficersRepository officersRepository)
        {
            _detaineeRepository = detaineeRepository;
            _arrestRepository = arrestRepository;
            _officersRepository = officersRepository;
        }

        public IEnumerable<Detainee> GetDetainees()
        {
            return _detaineeRepository.GetDetainees();
        }

        public Detainee GetDetaineeById(int detaineeId)
        {
            Detainee detainee = _detaineeRepository.GetDetaineeById(detaineeId);

            IEnumerable<Arrest> arrests = _arrestRepository.GetArrestsByDetaineeId(detaineeId);

            foreach (var arrest in arrests)
            {
                IEnumerable<Officer> officersForArrest = _officersRepository.GetOfficersByArrestId(arrest.ArrestId.Value);

                arrest.Officers = officersForArrest;
            }

            detainee.Arrests = arrests;

            return detainee;
        }

        public void DeleteDetaineeById(int detaineeId, string pathToPhotos)
        {
            IEnumerable<Arrest> arrests = _arrestRepository.GetArrestsByDetaineeId(detaineeId);

            foreach (var arrest in arrests)
            {
                _arrestRepository.DeleteArrestByArrestId(arrest.ArrestId.Value);
            }

            _detaineeRepository.DeleteDatainee(detaineeId, pathToPhotos);
        }

        public void DeleteArrestByArrestId(int arrestId)
        {
            _arrestRepository.DeleteArrestByArrestId(arrestId);
        }

        public void UpdatePhoto(int detaineeId, string pathToPhoto)
        {
            _detaineeRepository.UpdatePhoto(detaineeId, pathToPhoto);
        }

        public void UpdateDetaineeInformation(Detainee detainee)
        {
            _detaineeRepository.UpdateDetaineeInformation(detainee);
        }

        public int CreateDetainee(Detainee detainee)
        {
            return _detaineeRepository.InsertDatainee(detainee);
        }

        public int InsertArrest(Arrest arrest)
        {
            return _arrestRepository.InsertArrest(arrest);
        }

        public void UpdateArrest(Arrest arrest)
        {
            _arrestRepository.UpdateArrest(arrest);
        }

        public Arrest GetArrestByArrestID(int arrestId)
        {
            var arrest = _arrestRepository.GetArrestByArrestId(arrestId);

            if (arrest != null)
            {
                arrest.Officers = _officersRepository.GetOfficersByArrestId(arrestId);
            }

            return arrest;
        }

        public List<Detainee> SearchDetainees(SearchViewModel model)
        {
            var detaineeList = _detaineeRepository.SearchDetainees(model);

            return detaineeList;
        }

        public IEnumerable<Officer> GetAvailableOfficers(IEnumerable<Officer> arrestOfficers = null)
        {
            if (arrestOfficers is null)
            {
                return _officersRepository.GetOfficers();
            }

            var allOfficers = _officersRepository.GetOfficers();

            return allOfficers.Except(arrestOfficers);
        }

        public Officer GetOfficerById(int officerId)
        {
            return _officersRepository.GetOfficerById(officerId);
        }

        public void InsertOfficerForArrest(Officer officer, int arrestId)
        {
            _officersRepository.InsertOfficerForArrest(officer, arrestId);
        }

        public IEnumerable<int> GetLinks(int arrestId)
        {
            return _officersRepository.GetLinks(arrestId);
        }

        public void AddLinks(int arrestId, IEnumerable<Officer> officers)
        {
            _officersRepository.AddLinks(arrestId, officers);
        }

        public void DeleteLinks(int arrestId, IEnumerable<Officer> officers)
        {
            _officersRepository.DeleteLinks(arrestId, officers);
        }

        public void CreateOfficer(Officer officer)
        {
            _officersRepository.CreateOfficer(officer);
        }

        public IEnumerable<Officer> GetOfficers()
        {
            return _officersRepository.GetOfficers();
        }

        public void DeleteOfficer(int officerId)
        {
            _officersRepository.DeleteOfficer(officerId);
        }

        public void UpdateOfficer(Officer officer)
        {
            _officersRepository.UpdateOfficer(officer);
        }
    }
}
