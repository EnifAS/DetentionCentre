﻿using System.Collections.Generic;
using DetentionCentre.Models;
using DetentionCentre.ViewModels;

namespace DetentionCentre.Services
{
    public interface IUserManagementService
    {
        int CreateUser(RegisterModel registerModel);

        bool IsUserExist(string login, string email);

        User GetUser(string login, string password);

        User GetUser(int id);

        IEnumerable<User> GetUsers();

        IEnumerable<Role> GetRoles();

        void SaveChanges(User user);

        void Delete(int userId);

        void AddUser(UserEditViewModel model);
    }
}
