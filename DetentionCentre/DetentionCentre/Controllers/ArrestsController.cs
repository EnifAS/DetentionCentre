﻿using DetentionCentre.Models;
using DetentionCentre.Services;
using DetentionCentre.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace DetentionCentre.Controllers
{
    [Authorize(Roles = "Admin, Editor")]
    public class ArrestsController : Controller
    {
        private readonly IDetentionManagementService _detentionService;

        public ArrestsController(IDetentionManagementService detentionService)
        {
            _detentionService = detentionService;
        }

        [HttpGet]
        public IActionResult Add(int detaineeId)
        {
            ViewData["CurrentPage"] = "Home";

            return View("Edit", new ArrestEditViewModel
            {
                DetaineeId = detaineeId,
                Officers = new List<Officer>(),
                AvailableOfficers = _detentionService.GetAvailableOfficers()
            });
        }

        [HttpGet]
        public IActionResult Edit(int arrestId)
        {
            ViewData["CurrentPage"] = "Home";

            var arrest = _detentionService.GetArrestByArrestID(arrestId);

            return View("Edit", new ArrestEditViewModel
            {
                ArrestId = arrest.ArrestId,
                DeliveryDate = arrest.DeliveryDate,
                DetaineeId = arrest.DetaineeId,
                DetentionDate = arrest.DetentionDate,
                DetentionPlace = arrest.DetentionPlace,
                EstimatedAmount = arrest.EstimatedAmount?.ToString("0.00", CultureInfo.InvariantCulture),
                Officers = arrest.Officers.ToList(),
                PaidAmount = arrest.PaidAmount?.ToString("0.00", CultureInfo.InvariantCulture),
                ReleaseDate = arrest.ReleaseDate,

                AvailableOfficers = _detentionService.GetAvailableOfficers(arrest.Officers)
            });
        }

        [HttpGet]
        public IActionResult Delete(int arrestId, int detaineeId)
        {
            _detentionService.DeleteArrestByArrestId(arrestId);

            return RedirectToAction("Details", "Home", new { id = detaineeId });
        }

        [HttpPost]
        public IActionResult SaveChanges(ArrestEditViewModel arrestEditViewModel)
        {
            if (!ModelState.IsValid)
            {
                arrestEditViewModel.AvailableOfficers = _detentionService.GetAvailableOfficers(arrestEditViewModel.Officers);

                return View("Edit", arrestEditViewModel);
            }

            int arrestId;
            decimal? estimatedAmount = null;
            decimal? paidAmount = null;

            if (decimal.TryParse(arrestEditViewModel.EstimatedAmount, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out decimal estimatedAmountValue))
            {
                estimatedAmount = estimatedAmountValue;
            }

            if (decimal.TryParse(arrestEditViewModel.PaidAmount, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out decimal paidAmountValue))
            {
                paidAmount = paidAmountValue;
            }

            var arrest = new Arrest
            {
                DeliveryDate = arrestEditViewModel.DeliveryDate,
                DetaineeId = arrestEditViewModel.DetaineeId,
                DetentionDate = arrestEditViewModel.DetentionDate,
                DetentionPlace = arrestEditViewModel.DetentionPlace,
                EstimatedAmount = estimatedAmount,
                Officers = arrestEditViewModel.Officers,
                PaidAmount = paidAmount,
                ReleaseDate = arrestEditViewModel.ReleaseDate
            };

            if (arrestEditViewModel.ArrestId.HasValue)
            {
                arrest.ArrestId = arrestEditViewModel.ArrestId.Value;

                var previousOfficersCount = _detentionService.GetLinks(arrest.ArrestId.Value).Count();

                if (previousOfficersCount < arrest.Officers.Count())
                {
                    _detentionService.AddLinks(arrest.ArrestId.Value, arrest.Officers);
                }
                else if (previousOfficersCount > arrest.Officers.Count())
                {
                    _detentionService.DeleteLinks(arrest.ArrestId.Value, arrest.Officers);
                }

                _detentionService.UpdateArrest(arrest);

                arrestId = arrest.ArrestId.Value;
            }
            else
            {
                int newArrestId = _detentionService.InsertArrest(arrest);

                if (arrest.Officers != null)
                {
                    foreach (var officer in arrest.Officers)
                    {
                        _detentionService.InsertOfficerForArrest(officer, newArrestId);
                    }
                }

                arrestId = newArrestId;
            }

            return RedirectToAction("Edit", new { arrestId });
        }

        [HttpPost]
        public IActionResult AddOfficer(ArrestEditViewModel arrestEditViewModel)
        {
            ViewData["CurrentPage"] = "Home";

            List<Officer> officers = new List<Officer>();

            if (arrestEditViewModel.Officers != null)
            {
                officers = arrestEditViewModel.Officers.ToList();
            }

            if (arrestEditViewModel.SelectedOfficerId.HasValue)
            {
                var selectedOfficer = _detentionService.GetOfficerById(arrestEditViewModel.SelectedOfficerId.Value);

                officers.Add(selectedOfficer);

                arrestEditViewModel.Officers = officers;
            }

            arrestEditViewModel.AvailableOfficers = _detentionService.GetAvailableOfficers(arrestEditViewModel.Officers);
            arrestEditViewModel.SelectedOfficerId = null;

            return View("Edit", arrestEditViewModel);
        }

        [HttpPost]
        public IActionResult RemoveOfficer(ArrestEditViewModel arrestEditViewModel, int id)
        {
            ViewData["CurrentPage"] = "Home";

            var officer = arrestEditViewModel.Officers.FirstOrDefault(f => f.OfficerId == id);

            if (officer != null)
            {
                arrestEditViewModel.Officers.Remove(officer);
                arrestEditViewModel.AvailableOfficers = _detentionService.GetAvailableOfficers(arrestEditViewModel.Officers);
                arrestEditViewModel.SelectedOfficerId = null;
            }

            return View("Edit", arrestEditViewModel);
        }
    }
}
