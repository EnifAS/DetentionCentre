﻿using DetentionCentre.Models;
using DetentionCentre.Services;
using DetentionCentre.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace DetentionCentre.Controllers
{
    [Authorize(Roles = "Admin, Editor")]
    public class OfficersController : Controller
    {
        private readonly IDetentionManagementService _detentionService;

        public OfficersController(IDetentionManagementService detentionService)
        {
            _detentionService = detentionService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            ViewData["CurrentPage"] = "AddOfficer";

            var allOfficers = _detentionService.GetOfficers().ToList();

            var model = new OfficersViewModel
            {
                AllOfficers = allOfficers
            };

            return View("Officers", model);
        }

        [HttpPost]
        public IActionResult AddOfficer(OfficersViewModel officersViewModel)
        {
            var newOfficer = new Officer
            {
                LastName = officersViewModel.LastName,
                FirstName = officersViewModel.FirstName,
                MiddleName = officersViewModel.MiddleName,
                Title = officersViewModel.Title
            };

            _detentionService.CreateOfficer(newOfficer);

            var allOfficers = _detentionService.GetOfficers();

            return View("Officers", new OfficersViewModel
            {
                AllOfficers = allOfficers
            });
        }

        [HttpGet]
        public IActionResult Delete(int? id)
        {
            _detentionService.DeleteOfficer(id.Value);

            var allOfficers = _detentionService.GetOfficers();

            return View("Officers", new OfficersViewModel
            {
                AllOfficers = allOfficers
            });
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            ViewData["CurrentPage"] = "AddOfficer";

            var officer = _detentionService.GetOfficerById(id);

            return View("Officers", new OfficersViewModel
            {
                OfficerId = id,
                LastName = officer.LastName,
                FirstName = officer.FirstName,
                MiddleName = officer.MiddleName,
                Title = officer.Title,
                AllOfficers = new List<Officer>()
            });
        }

        [HttpPost]
        public IActionResult SaveChanges(OfficersViewModel officersViewModel)
        {
            var newData = new Officer
            {
                OfficerId = officersViewModel.OfficerId.Value,
                LastName = officersViewModel.LastName,
                FirstName = officersViewModel.FirstName,
                MiddleName = officersViewModel.MiddleName,
                Title = officersViewModel.Title
            };

            _detentionService.UpdateOfficer(newData);

            return RedirectToAction("Index");
        }
    }
}
