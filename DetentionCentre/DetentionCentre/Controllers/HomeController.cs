﻿using DetentionCentre.Models;
using DetentionCentre.Services;
using DetentionCentre.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace DetentionCentre.Controllers
{
    public class HomeController : Controller
    {
        private const string PhotosFolderName = "photos";

        private readonly IDetentionManagementService _detentionService;
        private readonly IWebHostEnvironment _appEnvironment;
        private readonly ILogger _logger;

        public HomeController(
            IWebHostEnvironment env,
            IDetentionManagementService detentionManagementService,
            ILogger<HomeController> logger)
        {
            _detentionService = detentionManagementService;
            _appEnvironment = env;
            _logger = logger;
        }

        [Authorize(Roles = "Admin, Editor, Guest")]
        public IActionResult Index()
        {
            ViewData["CurrentPage"] = "Home";

            var detainees = _detentionService.GetDetainees();

            return View(detainees);
        }

        [Authorize(Roles = "Admin, Editor, Guest")]
        [HttpGet]
        public IActionResult Details(int? id)
        {
            ViewData["CurrentPage"] = "Home";

            if (id == null)
            {
                return RedirectToAction("Index");
            }

            Detainee detainee = _detentionService.GetDetaineeById(id.Value);

            return View(detainee);
        }

        [Authorize(Roles = "Admin, Editor")]
        [HttpPost]
        public async Task<IActionResult> AddFile(FileUploadViewModel file)
        {
            if (file.UploadedFile != null)
            {
                var photoDirectoryPath = Path.Combine(_appEnvironment.WebRootPath, PhotosFolderName);

                if (!Directory.Exists(photoDirectoryPath))
                {
                    Directory.CreateDirectory(photoDirectoryPath);
                }

                var uploadedFileExtension = Path.GetExtension(file.UploadedFile.FileName);

                var fileName = $"{file.DetaineeId}{uploadedFileExtension}";

                using (var fileStream = new FileStream(Path.Combine(photoDirectoryPath, fileName), FileMode.Create))
                {
                    await file.UploadedFile.CopyToAsync(fileStream);
                }

                _detentionService.UpdatePhoto(file.DetaineeId, $"/{PhotosFolderName}/{fileName}");
            }

            return RedirectToAction("Details", new { id = file.DetaineeId });
        }

        [Authorize(Roles = "Admin, Editor")]
        [HttpPost]
        public IActionResult EditPersonalInformation(Detainee detainee)
        {
            ViewData["CurrentPage"] = "Home";

            if (ModelState.IsValid)
            {
                _detentionService.UpdateDetaineeInformation(detainee);

                return RedirectToAction("Details", new { id = detainee.DetaineeId });
            }

            return View("Details", detainee);
        }

        [Authorize(Roles = "Admin, Editor")]
        [HttpGet]
        public IActionResult AddDetainee()
        {
            ViewData["CurrentPage"] = "AddDetainee";

            return View("Detainee", new Detainee());
        }

        [Authorize(Roles = "Admin, Editor")]
        [HttpPost]
        public IActionResult CreateDetainee(Detainee detainee)
        {
            ViewData["CurrentPage"] = "AddDetainee";

            if (ModelState.IsValid)
            {
                var newDetaineeId = _detentionService.CreateDetainee(detainee);

                return RedirectToAction("Details", new { id = newDetaineeId });
            }

            return View("Detainee", detainee);
        }

        [Authorize(Roles = "Admin, Editor")]
        [HttpGet]
        public IActionResult DeleteDetainee(int id)
        {
            _detentionService.DeleteDetaineeById(id, Path.Combine(_appEnvironment.WebRootPath, PhotosFolderName));

            return RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            var path = HttpContext
              .Features
              .Get<IExceptionHandlerPathFeature>();
 
            var exception = path.Error;
            var pathString = path.Path;

            _logger.LogError(exception.Message);

            return View("../Shared/Error", new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
