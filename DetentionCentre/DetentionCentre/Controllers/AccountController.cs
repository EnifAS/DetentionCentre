﻿using DetentionCentre.Models;
using DetentionCentre.Services;
using DetentionCentre.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace RolesApp.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserManagementService _userManagementService;

        public AccountController(IUserManagementService userManagementService)
        {
            _userManagementService = userManagementService;
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                if (_userManagementService.IsUserExist(model.Login, model.Email))
                {
                    ModelState.AddModelError("", "User with the same login or email already exists.");
                }
                else
                {
                    int userId = _userManagementService.CreateUser(model); await Authenticate(new User
                    {
                        Id = userId,
                        Email = model.Email,
                        Login = model.Login
                    });

                    return RedirectToAction("Index", "Home");
                }
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var exististingUser = _userManagementService.GetUser(model.Login, model.Password);

                if (exististingUser != null)
                {
                    await Authenticate(exististingUser);

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Incorrect user name or password");
                }
            }

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await SignOut();

            return RedirectToAction("Index", "Home");
        }


        [HttpGet]
        public IActionResult Users()
        {
            ViewData["CurrentPage"] = "Users";

            var users = _userManagementService.GetUsers().ToList();

            return View("Users", users);
        }

        [HttpGet]
        public IActionResult EditUser(int id)
        {
            ViewData["CurrentPage"] = "Users";

            var user = _userManagementService.GetUser(id);

            var roles = _userManagementService.GetRoles();

            var viewModel = new UserEditViewModel
            {
                Id = user.Id,
                Login = user.Login,
                Password = user.Password,
                Email = user.Email,
                RoleId = user.RoleId,
                AvailableRoles = roles
            };

            return View("Edit", viewModel);
        }

        [HttpPost]
        public IActionResult SaveUser(UserEditViewModel model)
        {
            _userManagementService.SaveChanges(new User
            {
                Id = model.Id.Value,
                Login = model.Login,
                Password = model.Password,
                Email = model.Email,
                RoleId = model.RoleId
            });

            return RedirectToAction("Users");
        }

        [HttpGet]
        public IActionResult Delete(int userId)
        {
            _userManagementService.Delete(userId);

            return RedirectToAction("Users");
        }

        [HttpGet]
        public IActionResult Add()
        {
            ViewData["CurrentPage"] = "Users";

            return View("Edit", new UserEditViewModel { AvailableRoles = _userManagementService.GetRoles() });
        }

        [HttpPost]
        public IActionResult AddUser(UserEditViewModel model)
        {
            _userManagementService.AddUser(model);

            return RedirectToAction("Users");
        }

        private async Task Authenticate(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Login)
            };

            if (user.Role != null)
            {
                claims.Add(new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role.Name));
            }

            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);

            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }

        private async Task SignOut()
        {
            await HttpContext.SignOutAsync();
        }
    }
}
