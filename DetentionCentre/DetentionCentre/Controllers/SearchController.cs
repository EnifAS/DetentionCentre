﻿using DetentionCentre.Services;
using DetentionCentre.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DetentionCentre.Controllers
{
    [Authorize(Roles = "Admin, Editor, Guest")]
    public class SearchController : Controller
    {
        private readonly IDetentionManagementService _detentionService;

        public SearchController(IDetentionManagementService detentionService)
        {
            _detentionService = detentionService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            ViewData["CurrentPage"] = "Search";

            return View("Search", new SearchViewModel());
        }

        [HttpPost]
        public IActionResult GetSearchResult(SearchViewModel searchViewModel)
        {
            ViewData["CurrentPage"] = "Search";

            searchViewModel.DetaineesList = _detentionService.SearchDetainees(searchViewModel);

            return View("Search", searchViewModel);
        }
    }
}
