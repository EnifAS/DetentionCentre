﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DetentionCentre.Models
{
    public class Detainee
    {
        public int DetaineeId { get; set; }

        [Required(ErrorMessage = "Enter last name")]
        [MaxLength(50)]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Enter first name")]
        [MaxLength(50)]
        public string FirstName { get; set; }

        [MaxLength(50)]
        public string MiddleName { get; set; }

        [Required(ErrorMessage = "Indicate date of birth")]
        public DateTime? BirthDate { get; set; }

        [MaxLength(10)]
        public string MaritalStatus { get; set; }

        [Required(ErrorMessage = "Enter work place")]
        [MaxLength(100)]
        public string WorkPlace { get; set; }

        [Required(ErrorMessage = "Enter home adress")]
        [MaxLength(150)]
        public string HomeAdress { get; set; }

        [MaxLength(50)]
        public string PhoneNumber { get; set; }

        [MaxLength(50)]
        public string Photo { get; set; }

        [MaxLength(300)]
        public string Additional { get; set; }

        public IEnumerable<Arrest> Arrests { get; set; }

        public override int GetHashCode()
        {
            return DetaineeId;
        }

        public override bool Equals(object obj)
        {
            var incomingDetainee = obj as Detainee;

            if (this.DetaineeId == incomingDetainee.DetaineeId)
            {
                return true;
            }

            return false;
        }
    }
}
