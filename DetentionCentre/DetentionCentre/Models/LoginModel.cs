﻿using System.ComponentModel.DataAnnotations;

namespace DetentionCentre.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Please specify Login")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Please specify Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
