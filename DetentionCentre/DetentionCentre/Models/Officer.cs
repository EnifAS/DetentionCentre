﻿
using System.ComponentModel.DataAnnotations;

namespace DetentionCentre.Models
{
    public class Officer
    {
        public int OfficerId { get; set; }

        [Required(ErrorMessage = "Enter last name")]
        [MaxLength(50)]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Enter first name")]
        [MaxLength(50)]
        public string FirstName { get; set; }

        [MaxLength(50)]
        public string MiddleName { get; set; }

        [Required(ErrorMessage = "Enter title")]
        [MaxLength(50)]
        public string Title { get; set; }

        public string FullName => $"{FirstName} {LastName} [{Title}]";

        public override bool Equals(object obj)
        {
            var incomingOfficer = obj as Officer;

            if(this.OfficerId == incomingOfficer.OfficerId)
            {
                return true;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return OfficerId;
        }
    }
}
