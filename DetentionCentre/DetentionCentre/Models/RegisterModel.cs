﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DetentionCentre.Models
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "Please specify Login")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Please specify Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please specify Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Incorrect Password")]
        public string ConfirmPassword { get; set; }
    }
}
