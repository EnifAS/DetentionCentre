﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DetentionCentre.Models
{
    public class Arrest
    {
        public int? ArrestId { get; set; }

        [Required(ErrorMessage = "Indicate detention date")]
        public DateTime? DetentionDate { get; set; }

        [Required(ErrorMessage = "Indicate delivery date")]
        public DateTime? DeliveryDate { get; set; }

        public DateTime? ReleaseDate { get; set; }

        [Required(ErrorMessage = "Enter detention place")]
        [MaxLength(150)]
        public string DetentionPlace { get; set; }

        public decimal? EstimatedAmount { get; set; }

        public decimal? PaidAmount { get; set; }

        public int DetaineeId { get; set; }

        public IEnumerable<Officer> Officers { get; set; }
    }
}
