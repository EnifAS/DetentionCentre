﻿using DetentionCentre.Models;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;

namespace DetentionCentre.Repositories
{
    public class SqlOfficersRepository : BaseSqlRepository, IOfficersRepository
    {
        private const string IdColumn = "Id";
        private const string LastNameColumn = "LastName";
        private const string FirstNameColumn = "FirstName";
        private const string MiddleNameColumn = "MiddleName";
        private const string TitleDateColumn = "Title";

        public SqlOfficersRepository(IConfiguration configuration)
            : base(configuration)
        {
        }

        public IEnumerable<Officer> GetOfficers()
        {
            var officersResult = new List<Officer>();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlExpression = $"SELECT Id, LastName, FirstName, MiddleName, Title FROM Officers";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            object id = reader[IdColumn];
                            object lastName = reader[LastNameColumn];
                            object firstName = reader[FirstNameColumn];
                            object middleName = reader[MiddleNameColumn];
                            object title = reader[TitleDateColumn];

                            officersResult.Add(new Officer
                            {
                                OfficerId = (int)id,
                                LastName = lastName.ToString(),
                                FirstName = firstName.ToString(),
                                MiddleName = middleName.ToString(),
                                Title = title.ToString()
                            });
                        }
                    }
                }
            }

            return officersResult;
        }

        public IEnumerable<Officer> GetOfficersByArrestId(int arrestId)
        {
            var officersResult = new List<Officer>();

            List<int> officersIdList = (List<int>)GetLinks(arrestId);

            if (officersIdList.Count == 0)
            {
                return new List<Officer>();
            }

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlExpression = $"SELECT Id, LastName, FirstName, MiddleName, Title FROM Officers WHERE Id IN ({string.Join(',', officersIdList)})";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            object id = reader[IdColumn];
                            object lastName = reader[LastNameColumn];
                            object firstName = reader[FirstNameColumn];
                            object middleName = reader[MiddleNameColumn];
                            object title = reader[TitleDateColumn];

                            officersResult.Add(new Officer
                            {
                                OfficerId = (int)id,
                                LastName = lastName.ToString(),
                                FirstName = firstName.ToString(),
                                MiddleName = middleName.ToString(),
                                Title = title.ToString()
                            });
                        }
                    }
                }
            }

            return officersResult;
        }

        public IEnumerable<int> GetLinks(int arrestId)
        {
            var linksResult = new List<int>();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                string sqlExpression = $"SELECT OfficerId FROM LinkOfficersToArrests WHERE ArrestId = @Id";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(GetInputIntParameter("Id", arrestId));

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            object officerId = reader["OfficerId"];

                            linksResult.Add((int)officerId);
                        }
                    }
                }
            }

            return linksResult;
        }

        public void AddLinks(int arrestId, IEnumerable<Officer> officers)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                foreach (var officer in officers)
                {
                    string sqlExpression = $"SELECT Id FROM LinkOfficersToArrests WHERE ArrestId = @ArrestId AND OfficerId = @OfficerId";

                    SqlCommand command = new SqlCommand(sqlExpression, connection);

                    command.Parameters.Add(GetInputIntParameter("ArrestId", arrestId));
                    command.Parameters.Add(GetInputIntParameter("OfficerId", officer.OfficerId));

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.HasRows)
                        {
                            InsertLinks(officer.OfficerId, arrestId);
                        }
                    }
                }
            }
        }

        public void DeleteLinks(int arrestId, IEnumerable<Officer> officers)
        {
            var officersId = GetLinks(arrestId).ToList();

            foreach (var officer in officers)
            {
                officersId.Remove(officer.OfficerId);
            }

            foreach (var officer in officers)
            {
                DeleteLinkByOfficerId(officer.OfficerId, arrestId);
            }
        }

        public void CreateOfficer(Officer officer)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                string sqlExpression = $"INSERT INTO Officers VALUES (@LastName, @FirstName, @MiddleName, @Title)";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(GetInputStringParameter("LastName", officer.LastName));
                command.Parameters.Add(GetInputStringParameter("FirstName", officer.FirstName));
                command.Parameters.Add(GetInputStringParameter("MiddleName", officer.MiddleName));
                command.Parameters.Add(GetInputStringParameter("Title", officer.Title));

                command.ExecuteNonQuery();
            }
        }

        public void InsertOfficerForArrest(Officer officer, int arrestId)
        {
            InsertLinks(officer.OfficerId, arrestId);
        }

        public void InsertLinks(int officerId, int arrestId)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlExpression = $"INSERT INTO LinkOfficersToArrests VALUES (@officerId, @arrestId)";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(GetInputIntParameter("officerId", officerId));
                command.Parameters.Add(GetInputIntParameter("arrestId", arrestId));

                command.ExecuteNonQuery();
            }
        }

        public void DeleteOfficer(int officerId)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                DeleteLinkByOfficerId(officerId);

                var sqlExpression = $"DELETE FROM Officers WHERE Id = @OfficerId";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(GetInputIntParameter("OfficerId", officerId));

                command.ExecuteNonQuery();
            }
        }

        private void DeleteLinkByOfficerId(int officerId, int? arrestId = null)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                string sqlExpression = null;

                if (arrestId is null)
                {
                    sqlExpression = $"DELETE FROM LinkOfficersToArrests WHERE OfficerId = @OfficerId";
                }
                else
                {
                    sqlExpression = $"DELETE FROM LinkOfficersToArrests WHERE ArrestId = @ArrestId OfficerId = @OfficerId";
                }

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                if (arrestId is null)
                {
                    command.Parameters.Add(GetInputIntParameter("ArrestId", null));
                }
                else
                {
                    command.Parameters.Add(GetInputIntParameter("ArrestId", arrestId.Value));
                }

                command.Parameters.Add(GetInputIntParameter("OfficerId", officerId));

                command.ExecuteNonQuery();
            }
        }

        public Officer GetOfficerById(int officerId)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlExpression = $"SELECT Id, LastName, FirstName, MiddleName, Title FROM Officers WHERE Id = @Id";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(GetInputIntParameter("Id", officerId));

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            object id = reader[IdColumn];
                            object lastName = reader[LastNameColumn];
                            object firstName = reader[FirstNameColumn];
                            object middleName = reader[MiddleNameColumn];
                            object title = reader[TitleDateColumn];

                            return new Officer
                            {
                                OfficerId = (int)id,
                                LastName = lastName.ToString(),
                                FirstName = firstName.ToString(),
                                MiddleName = middleName.ToString(),
                                Title = title.ToString()
                            };
                        }
                    }
                }
            }

            return null;
        }

        public void UpdateOfficer(Officer officer)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlExpression = "UPDATE Officers SET " +
                    "LastName = @LastName, " +
                    "FirstName = @FirstName, " +
                    "MiddleName = @MiddleName, " +
                    "Title = @Title WHERE Id = @Id";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(GetInputStringParameter("LastName", officer.LastName));
                command.Parameters.Add(GetInputStringParameter("FirstName", officer.FirstName));
                command.Parameters.Add(GetInputStringParameter("MiddleName", officer.MiddleName));
                command.Parameters.Add(GetInputStringParameter("Title", officer.Title));
                command.Parameters.Add(GetInputIntParameter("Id", officer.OfficerId));

                command.ExecuteNonQuery();
            }
        }
    }
}
