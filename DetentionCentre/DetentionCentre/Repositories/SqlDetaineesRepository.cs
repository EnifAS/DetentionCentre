﻿using DetentionCentre.Extensions;
using DetentionCentre.Models;
using DetentionCentre.ViewModels;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DetentionCentre.Repositories
{
    public class SqlDetaineesRepository : BaseSqlRepository, IDetaineeRepository
    {
        private const string IdColumn = "Id";
        private const string LastNameColumn = "LastName";
        private const string FirstNameColumn = "FirstName";
        private const string MiddleNameColumn = "MiddleName";
        private const string BirthDateColumn = "BirthDate";
        private const string MaritalStatusColumn = "MaritalStatus";
        private const string WorkPlaceColumn = "WorkPlace";
        private const string HomeAddressColumn = "HomeAdress";
        private const string PhoneNumberColumn = "PhoneNumber";
        private const string PhotoColumn = "Photo";
        private const string AdditionalColumn = "Additional";

        public SqlDetaineesRepository(IConfiguration configuration)
            : base(configuration)
        {
        }

        public IEnumerable<Detainee> GetDetainees()
        {
            var detaineeResult = new List<Detainee>();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlExpression = "SELECT Id, LastName, FirstName, MiddleName FROM Detainees";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            object id = reader[IdColumn];
                            object lastName = reader[LastNameColumn];
                            object firstName = reader[FirstNameColumn];
                            object middleName = reader[MiddleNameColumn];

                            detaineeResult.Add(new Detainee
                            {
                                DetaineeId = (int)id,
                                LastName = lastName.ToString(),
                                FirstName = firstName.ToString(),
                                MiddleName = middleName.ToString()
                            });
                        }
                    }
                }
            }

            return detaineeResult;
        }

        public Detainee GetDetaineeById(int detaineeId)
        {
            Detainee detaineeResult = null;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlExpression = $"SELECT Id, LastName, FirstName, MiddleName, BirthDate, MaritalStatus, WorkPlace, HomeAdress, PhoneNumber, Photo, Additional FROM Detainees WHERE Id = @IdParam";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(GetInputIntParameter("IdParam", detaineeId));

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            object id = reader[IdColumn];
                            object lastName = reader[LastNameColumn];
                            object firstName = reader[FirstNameColumn];
                            object middleName = reader[MiddleNameColumn];
                            object birthDate = reader[BirthDateColumn];
                            object maritalStatus = reader[MaritalStatusColumn];
                            object workPlace = reader[WorkPlaceColumn];
                            object homeAdress = reader[HomeAddressColumn];
                            object phoneNumber = reader[PhoneNumberColumn];
                            object photo = reader[PhotoColumn];
                            object additional = reader[AdditionalColumn];

                            detaineeResult = new Detainee
                            {
                                DetaineeId = (int)id,
                                LastName = lastName.ToString(),
                                FirstName = firstName.ToString(),
                                MiddleName = middleName.ToString(),
                                BirthDate = DateTime.Parse(birthDate.ToString()),
                                MaritalStatus = maritalStatus.ToString(),
                                WorkPlace = workPlace.ToString(),
                                HomeAdress = homeAdress.ToString(),
                                PhoneNumber = phoneNumber.ToString(),
                                Photo = photo.ToString(),
                                Additional = additional.ToString()
                            };
                        }
                    }
                }
            }

            return detaineeResult;
        }

        public int InsertDatainee(Detainee detainee)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlExpression = "INSERT INTO Detainees OUTPUT INSERTED.Id VALUES (@LastName, @FirstName, @MiddleName, @BirthDate, @MaritalStatus, @WorkPlace, @HomeAdress, @PhoneNumber, @Photo, @Additional)";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(GetInputStringParameter(LastNameColumn, detainee.LastName));
                command.Parameters.Add(GetInputStringParameter(FirstNameColumn, detainee.FirstName));
                command.Parameters.Add(GetInputStringParameter(MiddleNameColumn, detainee.MiddleName));
                command.Parameters.Add(GetInputDateTimeParameter(BirthDateColumn, detainee.BirthDate));
                command.Parameters.Add(GetInputStringParameter(MaritalStatusColumn, detainee.MaritalStatus));
                command.Parameters.Add(GetInputStringParameter(WorkPlaceColumn, detainee.WorkPlace));
                command.Parameters.Add(GetInputStringParameter(HomeAddressColumn, detainee.HomeAdress));
                command.Parameters.Add(GetInputStringParameter(PhoneNumberColumn, detainee.PhoneNumber));
                command.Parameters.Add(GetInputStringParameter(PhotoColumn, detainee.Photo));
                command.Parameters.Add(GetInputStringParameter(AdditionalColumn, detainee.Additional));

                var data = command.ExecuteScalar();

                return (int)data;
            }
        }

        public void DeleteDatainee(int detaineeId, string pathToPhotos)
        {
            var pathToPhoto = GetPhotoPath(detaineeId);

            var fileExtention = Path.GetExtension(pathToPhoto);

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlExpression = $"DELETE FROM Detainees WHERE Id = @Id";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(GetInputIntParameter("Id", detaineeId));

                command.ExecuteNonQuery();
            }

            File.Delete(Path.Combine(pathToPhotos, detaineeId.ToString()) + $"{fileExtention}");
        }

        private string GetPhotoPath(int detaineeId)
        {
            string photoPath = null;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlExpression = $"SELECT Photo FROM Detainees WHERE Id = @Id";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(GetInputIntParameter("Id", detaineeId));

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            object photo = reader["Photo"];

                            photoPath = photo.ToString();
                        }
                    }
                }
            }

            return photoPath;
        }

        public void UpdatePhoto(int detaineeId, string pathToPhoto)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlExpression = $"UPDATE Detainees SET Photo = @PathToPhoto WHERE Id = @DetaineeId";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(GetInputStringParameter("PathToPhoto", pathToPhoto));
                command.Parameters.Add(GetInputStringParameter("DetaineeId", detaineeId));

                command.ExecuteNonQuery();
            }
        }

        public void UpdateDetaineeInformation(Detainee detainee)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlExpression = "UPDATE Detainees SET " +
                    "LastName = @detaineeLastName, " +
                    "FirstName = @detaineeFirstName, " +
                    "MiddleName = @detaineeMiddleName, " +
                    "BirthDate = @detaineeBirthDate, " +
                    "MaritalStatus = @detaineeMaritalStatus, " +
                    "WorkPlace = @detaineeWorkPlace, " +
                    "HomeAdress = @detaineeHomeAdress, " +
                    "PhoneNumber = @detaineePhoneNumber, " +
                    "Additional = @detaineeAdditional " +
                    "WHERE Id = @detaineeDetaineeId";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(GetInputStringParameter("detaineeLastName", detainee.LastName));
                command.Parameters.Add(GetInputStringParameter("detaineeFirstName", detainee.FirstName));
                command.Parameters.Add(GetInputStringParameter("detaineeMiddleName", detainee.MiddleName));
                command.Parameters.Add(GetInputDateTimeParameter("detaineeBirthDate", detainee.BirthDate));
                command.Parameters.Add(GetInputStringParameter("detaineeMaritalStatus", detainee.MaritalStatus));
                command.Parameters.Add(GetInputStringParameter("detaineeWorkPlace", detainee.WorkPlace));
                command.Parameters.Add(GetInputStringParameter("detaineeHomeAdress", detainee.HomeAdress));
                command.Parameters.Add(GetInputStringParameter("detaineePhoneNumber", detainee.PhoneNumber));
                command.Parameters.Add(GetInputStringParameter("detaineeAdditional", detainee.Additional));
                command.Parameters.Add(GetInputIntParameter("detaineeDetaineeId", detainee.DetaineeId));

                command.ExecuteNonQuery();
            }
        }

        public List<Detainee> SearchDetainees(SearchViewModel model)
        {
            var selectedByLastName = SearchByCriterion(LastNameColumn, model.LastName);
            var selectedByFirstName = SearchByCriterion(FirstNameColumn, model.FirstName);
            var selectedByMiddleName = SearchByCriterion(MiddleNameColumn, model.MiddleName);
            var selectedByHomeAdress = SearchByCriterion(HomeAddressColumn, model.HomeAdress);

            var selectedDetaineesId = model.DetentionDate == null ? new List<int>() : GetDetaineesId(model.DetentionDate.Value);
            var selectedById = new List<Detainee>();

            foreach (var id in selectedDetaineesId)
            {
                List<Detainee> searchResult = SearchByCriterion(IdColumn, id.ToString());

                selectedById.Add(searchResult[0]);
            }

            IEnumerable<Detainee> foundDetainees = selectedByLastName
                .IntersectNonEmpty(selectedByFirstName)
                .IntersectNonEmpty(selectedByMiddleName)
                .IntersectNonEmpty(selectedByHomeAdress)
                .IntersectNonEmpty(selectedById);

            return foundDetainees.ToList();
        }

        private List<Detainee> SearchByCriterion(string sqlColumn, string comparisonValue)
        {
            List<Detainee> detaineeResult = new List<Detainee>();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                if (comparisonValue == null)
                {
                    return new List<Detainee>();
                }

                connection.Open();

                var sqlExpression = $"SELECT Id, LastName, FirstName, MiddleName FROM Detainees WHERE {sqlColumn} LIKE @comparisonValue";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(GetInputStringParameter("comparisonValue", $"%{comparisonValue}%"));

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            object id = reader[IdColumn];
                            object lastName = reader[LastNameColumn];
                            object firstName = reader[FirstNameColumn];
                            object middleName = reader[MiddleNameColumn];

                            detaineeResult.Add(new Detainee
                            {
                                DetaineeId = (int)id,
                                LastName = lastName.ToString(),
                                FirstName = firstName.ToString(),
                                MiddleName = middleName.ToString()
                            });
                        }
                    }
                }
            }

            return detaineeResult;
        }

        private IEnumerable<int> GetDetaineesId(DateTime date)
        {
            var searchResult = new List<int>();

            var nextDay = date.AddDays(1);

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlExpression = $"SELECT DetaineeId FROM Arrests WHERE DetentionDate >= @dateFrom AND DetentionDate < @dateTo";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(GetInputDateTimeParameter("dateFrom", date));
                command.Parameters.Add(GetInputDateTimeParameter("dateTo", nextDay));

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            object detaineeId = reader["DetaineeId"];

                            searchResult.Add((int)detaineeId);
                        }
                    }
                }
            }

            return searchResult;
        }
    }
}
