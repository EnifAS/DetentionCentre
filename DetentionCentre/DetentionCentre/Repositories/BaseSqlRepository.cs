﻿using System;
using System.Data;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace DetentionCentre.Repositories
{
    public abstract class BaseSqlRepository
    {
        protected string _connectionString;

        public BaseSqlRepository(IConfiguration configuration)
        {
            _connectionString = configuration["ConnectionStrings:DefaultConnection"];
        }

        protected SqlParameter GetInputDateTimeParameter(string paramName, object paramValue)
        {
            SqlParameter sqlDateParam = new SqlParameter
            {
                DbType = DbType.DateTime,
                ParameterName = paramName,
                Value = paramValue ?? DBNull.Value,
                Direction = ParameterDirection.Input
            };

            return sqlDateParam;
        }

        protected SqlParameter GetInputStringParameter(string paramName, object paramValue)
        {
            SqlParameter sqlDateParam = new SqlParameter
            {
                DbType = DbType.String,
                ParameterName = paramName,
                Value = paramValue ?? DBNull.Value,
                Direction = ParameterDirection.Input
            };

            return sqlDateParam;
        }

        protected SqlParameter GetInputIntParameter(string paramName, object paramValue)
        {
            SqlParameter sqlDateParam = new SqlParameter
            {
                DbType = DbType.Int32,
                ParameterName = paramName,
                Value = paramValue ?? DBNull.Value,
                Direction = ParameterDirection.Input
            };

            return sqlDateParam;
        }

        protected SqlParameter GetInputDecimalParameter(string paramName, object paramValue)
        {
            SqlParameter sqlDateParam = new SqlParameter
            {
                DbType = DbType.Decimal,
                ParameterName = paramName,
                Value = paramValue ?? DBNull.Value,
                Direction = ParameterDirection.Input
            };

            return sqlDateParam;
        }

        protected T? ParseNullableDbType<T>(object dbObject) where T : struct
        {
            if(dbObject is DBNull)
            {
                return null;
            }

            return (T)dbObject;
        }
    }
}
