﻿using DetentionCentre.Models;
using System;
using System.Collections.Generic;

namespace DetentionCentre.Repositories
{
    public interface IOfficersRepository
    {
        IEnumerable<Officer> GetOfficers();

        Officer GetOfficerById(int officerId);

        IEnumerable<Officer> GetOfficersByArrestId(int arrestId);

        public IEnumerable<int> GetLinks(int arrestId);

        void CreateOfficer(Officer officer);

        void InsertOfficerForArrest(Officer officer, int arrestId);

        public void AddLinks(int arrestId, IEnumerable<Officer> officers);

        public void DeleteLinks(int arrestId, IEnumerable<Officer> officers);

        void DeleteOfficer(int officerId);

        void UpdateOfficer(Officer officer);
    }
}
