﻿using DetentionCentre.Models;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;

namespace DetentionCentre.Repositories
{
    public class SqlArrestsRepository : BaseSqlRepository, IArrestRepository
    {
        private const string IdColumn = "Id";
        private const string DetentionDateColumn = "DetentionDate";
        private const string DeliveryDateColumn = "DeliveryDate";
        private const string ReleaseDateColumn = "ReleaseDate";
        private const string DetaineeIdColumn = "DetaineeId";
        private const string DetentionPlaceColumn = "DetentionPlace";
        private const string EstimatedAmountColumn = "EstimatedAmount";
        private const string PaidAmountColumn = "PaidAmount";

        public SqlArrestsRepository(IConfiguration configuration)
            : base(configuration)
        {
        }

        public IEnumerable<Arrest> GetArrestsByDetaineeId(int detaineeId)
        {
            var arrestResult = new List<Arrest>();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlExpression = $"SELECT Id, DetentionDate, DeliveryDate, ReleaseDate, DetentionPlace, EstimatedAmount, PaidAmount FROM Arrests WHERE DetaineeID = @Id";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(GetInputIntParameter("Id", detaineeId));

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            object id = reader[IdColumn];
                            object detentionDate = reader[DetentionDateColumn];
                            object deliveryDate = reader[DeliveryDateColumn];
                            object releaseDate = ParseNullableDbType<DateTime>(reader[ReleaseDateColumn]);
                            object detantionPlace = reader[DetentionPlaceColumn];
                            object estimatedAmount = ParseNullableDbType<decimal>(reader[EstimatedAmountColumn]);
                            object paidAmount = ParseNullableDbType<decimal>(reader[PaidAmountColumn]);

                            arrestResult.Add(new Arrest
                            {
                                ArrestId = (int)id,
                                DetentionDate = (DateTime)detentionDate,
                                DeliveryDate = (DateTime)deliveryDate,
                                ReleaseDate = (DateTime?)releaseDate,
                                DetentionPlace = detantionPlace.ToString(),
                                EstimatedAmount = (decimal?)estimatedAmount,
                                PaidAmount = (decimal?)paidAmount
                            });
                        }
                    }
                }
            }

            return arrestResult;
        }

        public void DeleteArrestByArrestId(int arrestId)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                DeleteLinkByArrestId(arrestId);

                var sqlExpression = $"DELETE FROM Arrests WHERE Id = @Id";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(GetInputIntParameter("Id", arrestId));

                command.ExecuteNonQuery();
            }
        }

        private void DeleteLinkByArrestId(int arrestId)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlExpression = $"DELETE FROM LinkOfficersToArrests WHERE ArrestId = @Id";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(GetInputIntParameter("Id", arrestId));

                command.ExecuteNonQuery();
            }
        }

        public int InsertArrest(Arrest arrest)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlExpression = "INSERT Arrests OUTPUT INSERTED.Id VALUES (@DetentionDate, @DeliveryDate, @ReleaseDate, @DetaineeId, @DetentionPlace, @EstimatedAmount, @PaidAmount)";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(GetInputDateTimeParameter("DetentionDate", arrest.DetentionDate));
                command.Parameters.Add(GetInputDateTimeParameter("DeliveryDate", arrest.DeliveryDate));
                command.Parameters.Add(GetInputDateTimeParameter("ReleaseDate", arrest.ReleaseDate));
                command.Parameters.Add(GetInputIntParameter("DetaineeId", arrest.DetaineeId));
                command.Parameters.Add(GetInputStringParameter("DetentionPlace", arrest.DetentionPlace));
                command.Parameters.Add(GetInputDecimalParameter("EstimatedAmount", arrest.EstimatedAmount));
                command.Parameters.Add(GetInputDecimalParameter("PaidAmount", arrest.PaidAmount));

                var data = command.ExecuteScalar();

                return (int)data;
            }
        }

        public void UpdateArrest(Arrest arrest)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlExpression = "UPDATE Arrests SET " +
                    "DetentionDate = @DetentionDate," +
                    "DeliveryDate = @DeliveryDate," +
                    "ReleaseDate = @ReleaseDate," +
                    "DetaineeId = @DetaineeId," +
                    "DetentionPlace = @DetentionPlace," +
                    "EstimatedAmount = @EstimatedAmount," +
                    "PaidAmount = @PaidAmount;";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(GetInputDateTimeParameter("DetentionDate", arrest.DetentionDate));
                command.Parameters.Add(GetInputDateTimeParameter("DeliveryDate", arrest.DeliveryDate));
                command.Parameters.Add(GetInputDateTimeParameter("ReleaseDate", arrest.ReleaseDate));
                command.Parameters.Add(GetInputIntParameter("DetaineeId", arrest.DetaineeId));
                command.Parameters.Add(GetInputStringParameter("DetentionPlace", arrest.DetentionPlace));
                command.Parameters.Add(GetInputDecimalParameter("EstimatedAmount", arrest.EstimatedAmount));
                command.Parameters.Add(GetInputDecimalParameter("PaidAmount", arrest.PaidAmount));

                command.ExecuteNonQuery();
            }
        }

        public Arrest GetArrestByArrestId(int arrestId)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlExpression = $"SELECT Id, DetentionDate, DeliveryDate, ReleaseDate, DetaineeId, DetentionPlace, EstimatedAmount, PaidAmount FROM Arrests WHERE Id = @Id";

                Arrest arrest = null;

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(GetInputIntParameter("Id", arrestId));

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            object id = reader[IdColumn];
                            object detentionDate = reader[DetentionDateColumn];
                            object deliveryDate = reader[DeliveryDateColumn];
                            object releaseDate = reader[ReleaseDateColumn];
                            object detaineeId = reader[DetaineeIdColumn];
                            object detantionPlace = reader[DetentionPlaceColumn];
                            object estimatedAmount = reader[EstimatedAmountColumn];
                            object paidAmount = reader[PaidAmountColumn];

                            arrest = new Arrest
                            {
                                ArrestId = (int)id,
                                DetentionDate = (DateTime)detentionDate,
                                DeliveryDate = (DateTime)deliveryDate,
                                ReleaseDate = ParseNullableDbType<DateTime>(releaseDate),
                                DetaineeId = (int)detaineeId,
                                DetentionPlace = detantionPlace.ToString(),
                                EstimatedAmount = ParseNullableDbType<decimal>(estimatedAmount),
                                PaidAmount = ParseNullableDbType<decimal>(paidAmount)
                            };
                        }
                    }
                }

                return arrest;
            }
        }
    }
}
