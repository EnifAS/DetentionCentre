﻿using DetentionCentre.Models;
using DetentionCentre.ViewModels;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace DetentionCentre.Repositories
{
    public class SqlUserRepository : BaseSqlRepository, IUserRepository
    {
        private const string NewUserRole = "Guest";
        private const string IdColumn = "Id";
        private const string LoginColumn = "Login";
        private const string PasswordColumn = "Password";
        private const string EmailColumn = "Email";
        private const string RoleIdColumn = "RoleId";
        private const string RoleNameColumn = "RoleName";

        public SqlUserRepository(IConfiguration configuration)
            : base(configuration)
        {
        }

        public int CreateUser(RegisterModel registerModel)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlExpressionForRole = $"SELECT Id FROM Roles WHERE RoleName = '{NewUserRole}'";

                SqlCommand commandForRole = new SqlCommand(sqlExpressionForRole, connection);

                var roleId = 0;

                using (SqlDataReader reader = commandForRole.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            object id = reader[IdColumn];

                            roleId = (int)id;
                        }
                    }
                }

                var sqlExpressionForUser = "INSERT INTO AppUsers OUTPUT INSERTED.Id VALUES (@Login, @Password, @Email, @RoleId)";

                SqlCommand commandForUser = new SqlCommand(sqlExpressionForUser, connection);

                commandForUser.Parameters.Add(GetInputStringParameter("Login", registerModel.Login));
                commandForUser.Parameters.Add(GetInputStringParameter("Password", registerModel.Password));
                commandForUser.Parameters.Add(GetInputStringParameter("Email", registerModel.Email));
                commandForUser.Parameters.Add(GetInputStringParameter("RoleId", roleId));

                var data = commandForUser.ExecuteScalar();

                return (int)data;
            }
        }

        public bool IsUserExist(string login, string email)
        {
            int? userId = null;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlExpression = "SELECT Id FROM AppUsers WHERE Login = @Login AND Email = @Email";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(GetInputStringParameter("Login", login));
                command.Parameters.Add(GetInputStringParameter("Email", email));

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            object id = reader["Id"];

                            userId = (int)id;
                        }
                    }
                }
            }

            if (userId.HasValue)
            {
                return true;
            }

            return false;
        }

        public User GetUser(string recievedLogin, string recievedPassword)
        {
            var user = new User();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlExpressionForUser = "SELECT Id, Login, Password, Email, RoleId FROM AppUsers WHERE Login = @Login AND Password = @Password";

                SqlCommand commandForUser = new SqlCommand(sqlExpressionForUser, connection);

                commandForUser.Parameters.Add(GetInputStringParameter("Login", recievedLogin));
                commandForUser.Parameters.Add(GetInputStringParameter("Password", recievedPassword));

                using (SqlDataReader reader = commandForUser.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            object id = reader[IdColumn];
                            object login = reader[LoginColumn];
                            object password = reader[PasswordColumn];
                            object email = reader[EmailColumn];
                            object roleId = reader[RoleIdColumn];

                            user.Id = (int)id;
                            user.Login = login.ToString();
                            user.Password = password.ToString();
                            user.Email = email.ToString();
                            user.RoleId = (int)roleId;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }

            user.Role = GetRoleById(user.RoleId.Value);

            return user;
        }

        public User GetUser(int userId)
        {
            var user = new User();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlExpression = "SELECT Id, Login, Password, Email, RoleId FROM AppUsers WHERE Id = @Id";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(GetInputIntParameter("Id", userId));

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            object id = reader[IdColumn];
                            object login = reader[LoginColumn];
                            object password = reader[PasswordColumn];
                            object email = reader[EmailColumn];
                            object roleId = reader[RoleIdColumn];

                            user.Id = (int)id;
                            user.Login = login.ToString();
                            user.Password = password.ToString();
                            user.Email = email.ToString();
                            user.RoleId = (int)roleId;
                            user.Role = GetRoleById((int)id);
                        }
                    }
                }
            }

            return user;
        }

        private Role GetRoleById(int id)
        {
            var role = new Role { Id = id };

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlExpressionForRole = "SELECT RoleName FROM Roles WHERE Id = @ID";

                SqlCommand commandForRole = new SqlCommand(sqlExpressionForRole, connection);

                commandForRole.Parameters.Add(GetInputStringParameter("ID", id));

                using (SqlDataReader reader = commandForRole.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            object roleName = reader[RoleNameColumn];

                            role.Name = roleName.ToString();
                        }
                    }
                }
            }

            return role;
        }

        public IEnumerable<User> GetUsers()
        {
            var users = new List<User>();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlExpression = "SELECT Id, Login, Password, Email, RoleId FROM AppUsers";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            object id = reader[IdColumn];
                            object login = reader[LoginColumn];
                            object password = reader[PasswordColumn];
                            object email = reader[EmailColumn];
                            object roleId = reader[RoleIdColumn];

                            users.Add(new User
                            {
                                Id = (int)id,
                                Login = login.ToString(),
                                Password = password.ToString(),
                                Email = email.ToString(),
                                RoleId = (int)roleId,
                                Role = GetRoleById((int)roleId)
                            });
                        }
                    }
                }
            }

            return users;
        }

        public IEnumerable<Role> GetRoles()
        {
            var users = new List<Role>();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlExpression = "SELECT Id, RoleName FROM Roles";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            object id = reader[IdColumn];
                            object roleName = reader[RoleNameColumn];

                            users.Add(new Role
                            {
                                Id = (int)id,
                                Name = roleName.ToString()
                            });
                        }
                    }
                }
            }

            return users;
        }

        public void SaveChanges(User user)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlExpression = $"UPDATE AppUsers SET Login = @Login, Email = @Email, RoleId = @Role WHERE Id = @Id";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(GetInputStringParameter("Login", user.Login));
                command.Parameters.Add(GetInputStringParameter("Email", user.Email));
                command.Parameters.Add(GetInputIntParameter("Role", user.RoleId));
                command.Parameters.Add(GetInputIntParameter("Id", user.Id));

                command.ExecuteNonQuery();
            }
        }

        public void Delete(int userId)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlExpression = $"DELETE FROM AppUsers WHERE Id = @Id";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(GetInputIntParameter("Id", userId));

                command.ExecuteNonQuery();
            }
        }

        public void AddUser(UserEditViewModel model)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var sqlExpression = "INSERT INTO AppUsers VALUES (@Login, @Password, @Email, @RoleId)";

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                command.Parameters.Add(GetInputStringParameter("Login", model.Login));
                command.Parameters.Add(GetInputStringParameter("Password", model.Password));
                command.Parameters.Add(GetInputStringParameter("Email", model.Email));
                command.Parameters.Add(GetInputIntParameter("RoleId", model.RoleId));

                command.ExecuteNonQuery();
            }
        }
    }
}
