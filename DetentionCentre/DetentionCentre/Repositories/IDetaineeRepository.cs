﻿using DetentionCentre.Models;
using DetentionCentre.ViewModels;
using System.Collections.Generic;

namespace DetentionCentre.Repositories
{
    public interface IDetaineeRepository
    {
        IEnumerable<Detainee> GetDetainees();

        Detainee GetDetaineeById(int detaineeId);

        int InsertDatainee(Detainee detainee);

        void DeleteDatainee(int detaineeId, string pathToPhotos);

        void UpdatePhoto(int detaineeId, string pathToPhoto);

        void UpdateDetaineeInformation(Detainee detainee);

        List<Detainee> SearchDetainees(SearchViewModel model);
    }
}
