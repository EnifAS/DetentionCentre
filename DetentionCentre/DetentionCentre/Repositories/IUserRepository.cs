﻿using DetentionCentre.Models;
using DetentionCentre.ViewModels;
using System.Collections.Generic;

namespace DetentionCentre.Repositories
{
    public interface IUserRepository
    {
        int CreateUser(RegisterModel registerModel);

        bool IsUserExist(string login, string email);

        User GetUser(string recievedLogin, string recievedPassword);

        User GetUser(int userId);

        IEnumerable<User> GetUsers();

        IEnumerable<Role> GetRoles();

        public void SaveChanges(User user);

        public void Delete(int userId);

        public void AddUser(UserEditViewModel model);
    }
}
