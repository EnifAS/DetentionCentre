﻿using DetentionCentre.Models;
using System.Collections.Generic;

namespace DetentionCentre.Repositories
{
    public interface IArrestRepository
    {
        IEnumerable<Arrest> GetArrestsByDetaineeId(int detaineeId);

        void DeleteArrestByArrestId(int arrestId);

        int InsertArrest(Arrest arrest);

        void UpdateArrest(Arrest arrest);

        Arrest GetArrestByArrestId(int arrestId);
    }
}
