CREATE TABLE Detainees
(
	Id INT PRIMARY KEY IDENTITY (1, 1),
	LastName NVARCHAR(50) NOT NULL,
	FirstName NVARCHAR(50) NOT NULL,
	MiddleName NVARCHAR(50) NULL,
	BirthDate DATE NOT NULL,
	MaritalStatus NVARCHAR(10) NULL,
	WorkPlace NVARCHAR(100) NOT NULL,
	HomeAdress NVARCHAR(150) NOT NULL,
	PhoneNumber VARCHAR(50) NULL,
	Photo NVARCHAR(50) NULL,
	Additional NVARCHAR(300) NULL, 
	CONSTRAINT CK_Detainee_BirthDate CHECK (BirthDate > '1920-01-01' AND BirthDate < GETDATE())
)

CREATE TABLE Arrests
(
	Id INT PRIMARY KEY IDENTITY (1, 1),
	DetentionDate DATETIME NOT NULL,
	DeliveryDate DATETIME NOT NULL,
	ReleaseDate DATETIME NULL,
	DetaineeId INT NOT NULL,
	DetentionPlace NVARCHAR(150) NOT NULL,
	EstimatedAmount MONEY NULL,
	PaidAmount MONEY NULL,
	CONSTRAINT CK_Arrest_DeliveryDate CHECK (DeliveryDate > DetentionDate),
	CONSTRAINT CK_Arrest_ReleaseDate CHECK (ReleaseDate > DeliveryDate),
	CONSTRAINT CK_Arrest_EstimatedAmount CHECK (EstimatedAmount >= 0),
	CONSTRAINT CK_Arrest_PaidAmount CHECK (PaidAmount >= 0),
	CONSTRAINT FK_Arrest_To_Detainees FOREIGN KEY (DetaineeId) REFERENCES Detainees (Id)
)

CREATE TABLE Officers
(
	Id INT PRIMARY KEY IDENTITY (1, 1),
	LastName NVARCHAR(50) NOT NULL,
	FirstName NVARCHAR(50) NOT NULL,
	MiddleName NVARCHAR(50) NULL,
	Title NVARCHAR(50) NOT NULL
)

CREATE TABLE LinkOfficersToArrests
(
	Id INT PRIMARY KEY IDENTITY (1, 1),
	OfficerId INT NOT NULL,
	ArrestId INT NOT NULL
	CONSTRAINT FK_LinkOfficerToArrest_To_Officers FOREIGN KEY (OfficerId) REFERENCES Officers (Id),
	CONSTRAINT FK_LinkOfficerToArrest_To_Arrests FOREIGN KEY (ArrestId) REFERENCES Arrests (Id)
)

CREATE TABLE Roles
(
	Id INT PRIMARY KEY IDENTITY (1, 1),
	RoleName NVARCHAR(50) NOT NULL
)

GO

INSERT INTO Roles VALUES
('Admin'),
('Editor'),
('Guest')

CREATE TABLE AppUsers
(
	Id INT PRIMARY KEY IDENTITY (1, 1),
	Login NVARCHAR(50) NOT NULL,
	Password NVARCHAR(100) NOT NULL,
	Email NVARCHAR(100) NOT NULL,
	RoleId INT NOT NULL,
	CONSTRAINT FK_Users_To_Roles FOREIGN KEY (RoleId) REFERENCES Roles (Id)
)

GO

INSERT INTO AppUsers VALUES
('admin', 'admin', 'admin@mail.ru', 1)